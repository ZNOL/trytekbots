# coding: utf-8

import logging
# FORMAT = '%(asctime)s %(processName)s\%(name)-8s %(levelname)s: %(message)s'
logfile = '/home/TrytekUserside/all_logs.txt'
logging.basicConfig(filename=logfile, encoding='utf-8', level=logging.INFO)


api_id = 
api_hash = 
bot_name = 
bot_token = 


link_url = 
api_url = 
main_root = '/home/TrytekUserside/'


DB_HOST = 
DB_SOCKET = 
DB_USER = 
DB_PASSWORD = 
DB_DATABASE = 'trytek_userside_xvi'


# Кол-во минут через которые удаляются оповещения
delete_minutes = 5


# при первой пересылке изменяется тип задачи [с какого меняется]: [на какой меняется],
config_change_type = {
    347: 348,
    365: 368,
    366: 384,
    367: 408,
    425: 425,
    426: 427,
}


# ID пользователей с доступом к формированию отчётов по складу
storekeepers = {
}


admins = {
    0,
}
